FROM ubuntu:20.04

# add gosu for easy step-down from root
# https://github.com/tianon/gosu/releases
ENV GOSU_VERSION 1.12
RUN set -eux; \
	# save list of currently installed packages for later so we can clean up
	savedAptMark="$(apt-mark showmanual)"; \
	apt-get update; \
	apt-get install -y --no-install-recommends ca-certificates wget; \
	if ! command -v gpg; then \
	apt-get install -y --no-install-recommends gnupg2 dirmngr; \
	elif gpg --version | grep -q '^gpg (GnuPG) 1\.'; then \
	# "This package provides support for HKPS keyservers." (GnuPG 1.x only)
	apt-get install -y --no-install-recommends gnupg-curl; \
	fi; \
	rm -rf /var/lib/apt/lists/*; \
	\
	dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
	wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch"; \
	wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc"; \
	\
	# verify the signature
	export GNUPGHOME="$(mktemp -d)"; \
	gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
	gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
	command -v gpgconf && gpgconf --kill all || :; \
	rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc; \
	\
	# clean up fetch dependencies
	apt-mark auto '.*' > /dev/null; \
	[ -z "$savedAptMark" ] || apt-mark manual $savedAptMark; \
	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
	\
	chmod +x /usr/local/bin/gosu; \
	# verify that the binary works
	gosu --version; \
	gosu nobody true

# 一些 測試工具 和開發 用戶

# Configuring tzdata
ENV DEBIAN_FRONTEND=noninteractive 

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends sudo ca-certificates git xz-utils curl wget bash-completion; \
	useradd -m -U -s /bin/bash dev; \
	echo "dev ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers; \
	rm -rf /var/lib/apt/lists/*;

RUN set -eux; \
	curl -#Lo /a.tar.xz https://nodejs.org/dist/v14.17.0/node-v14.17.0-linux-x64.tar.xz; \
	tar -Jxvf /a.tar.xz -C /opt/; \
	rm /a.tar.xz; \
	mv /opt/node-v14.17.0-linux-x64 /opt/node

RUN set -eux; \
	export PATH=$PATH:/opt/node/bin; \
	npm install -g @king011/jsgenerate; \
	mkdir /root/.jsgenerate/init -p; \
	jsgenerate complete > /etc/bash_completion.d/jsgenerate; \
	echo '# enable bash completion in interactive shells' >> /etc/bash.bashrc; \
	echo 'if ! shopt -oq posix; then' >> /etc/bash.bashrc; \
	echo ' if [ -f /usr/share/bash-completion/bash_completion ]; then' >> /etc/bash.bashrc; \
	echo '   . /usr/share/bash-completion/bash_completion' >> /etc/bash.bashrc; \
	echo ' elif [ -f /etc/bash_completion ]; then' >> /etc/bash.bashrc; \
	echo '   . /etc/bash_completion' >> /etc/bash.bashrc; \
	echo ' fi' >> /etc/bash.bashrc; \
	echo 'fi' >> /etc/bash.bashrc;

# template
RUN set -eux;\
	mkdir /home/dev/.jsgenerate/init -p; \
	git clone https://github.com/powerpuffpenguin/jsgenerate_grpc-gateway.git /home/dev/.jsgenerate/init/jsgenerate_grpc-gateway; \
	chown dev.dev /home/dev/.jsgenerate -R; 
RUN set -eux;\
	git clone https://gitlab.com/king011_cpp/cpp-console.git /home/dev/.jsgenerate/init/cpp-console; \
	chown dev.dev /home/dev/.jsgenerate/init/cpp-console -R; 

# 環境變量配置
RUN set -eux; \
	mkdir /home/dev/project; \
	chown dev.dev /home/dev/project;\
	echo "export PATH=\"$PATH:/opt/node/bin\"" >> /etc/profile; \
	echo "export PATH=\"$PATH:/opt/node/bin\"" >> /etc/bash.bashrc;

ENV  PATH=$PATH:/opt/node/bin 

VOLUME ["/home/dev/project"]

COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]

WORKDIR /home/dev/project
CMD ["bash"]