#!/usr/bin/env bash
set -ex
Dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
cd "$Dirname"

sudo docker build -t king011/template:latest .